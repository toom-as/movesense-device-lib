swagger: '2.0'

info:
  version: NA
  title: SensorHub - Movesense-API
  description: |
    This file defines general API for getting syncronised measurements from
    different kind and different amounts of sensors.
  x-api-type: public
  x-api-required: true

paths:

  /Meas/SensorHub/Info:
    get:
      description: Get information about the sensor hub API.
      responses:
        200:
          description: Returns sensors information.
          schema:
            $ref: '#/definitions/SensorHubInfo'

  /Meas/SensorHub/{SampleRate}/Subscription:
    parameters:
       - $ref: '#/parameters/SampleRate'
    post:
      description: |
        Subscribe to periodic sensor hub measurements.
      responses:
        200:
          description: Operation completed successfully
        501:
          description: Non-supported sample rate
        x-notification:
          description: New measurements
          schema:
            $ref: '#/definitions/SensorHubData'
    delete:
      description: |
        Unsubscribe from periodic sensor hub measurements.
      responses:
        200:
          description: Operation completed successfully

parameters:
  SampleRate:
    name: SampleRate
    in: path
    required: true
    type: integer
    format: int32

definitions:

  SensorHubInfo:
    properties:
      Accelerometers:
        description: |
          Array of available accelerometer sensors and their info.
        type: array
        items:
          $ref: 'meas/acc.yaml#/definitions/AccInfo'
      Gyroscopes:
        description: |
          Array of available gyroscope sensors
        type: array
        items:
          $ref: 'meas/gyro.yaml#/definitions/GyroInfo'
      Magnetometers:
        description: |
          Array of available magnetometer sensors
        type: array
        items:
          $ref: 'meas/magn.yaml#/definitions/MagnInfo'
      PressureSensors:
        description: |
          Array of available pressure sensors
        type: array
        items:
          $ref: 'meas/press.yaml#/definitions/PressInfo'

  SensorHubData:
    required:
      - Timestamp
    properties:
      Timestamp:
        description: Local timestamp of first measurement.
        type: integer
        format: uint32
        x-unit: millisecond
      Accelerometers:
        description: Accelerometers and their accelerations (in array).
        type: array
        items:
          $ref: 'meas/acc.yaml#/definitions/AccData'
      Gyroscopes:
        description: Gyroscopes and their angular velocities (in array).
        type: array
        items:
          $ref: 'meas/gyro.yaml#/definitions/GyroData'
      Magnetometers:
        description: Measured magnetic field values (3D) in array.
        type: array
        items:
          $ref: 'meas/magn.yaml#/definitions/MagnData'
      PressureSensors:
        description: Pressure sensors and their pressure values in array.
        type: array
        items:
          $ref: 'meas/press.yaml#/definitions/PressData'
