#pragma once
/******************************************************************************
    Copyright (c) Suunto Oy 2015.
    All rights reserved.
******************************************************************************/

#include "whiteboard/integration/shared/types.h"
#include "whiteboard/integration/shared/infinite.h"

// Define this in your make files / build system
#include WB_BSP

#include "whiteboard/integration/bsp/bsp.h"

#include "whiteboard/integration/os/os.h"
